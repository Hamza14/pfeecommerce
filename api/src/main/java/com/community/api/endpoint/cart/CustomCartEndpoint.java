/*
 * Copyright 2008-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.community.api.endpoint.cart;

import com.broadleafcommerce.rest.api.endpoint.catalog.CatalogEndpoint;
import com.broadleafcommerce.rest.api.endpoint.order.CartEndpoint;
import com.broadleafcommerce.rest.api.wrapper.OrderItemWrapper;
import com.broadleafcommerce.rest.api.wrapper.OrderWrapper;
import org.broadleafcommerce.profile.core.dao.CustomerDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * This is a reference REST API endpoint for cart. This can be modified, used as is, or removed.
 * The purpose is to provide an out of the box RESTful cart service implementation, but also
 * to allow the implementor to have fine control over the actual API, URIs, and general JAX-RS annotations.
 *
 * @author Kelly Tisdell
 */
@RestController
@RequestMapping(value = "/cart",
        produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class CustomCartEndpoint extends CartEndpoint {
    @Autowired
    CustomerDaoImpl customerDao;

    @Override
    @RequestMapping(value = "", method = RequestMethod.GET)
    public OrderWrapper findCartForCustomer(HttpServletRequest request) {
        try {
            return super.findCartForCustomer(request);
        } catch (Exception e) {
            // if we failed to find the cart, create a new one
            return createNewCartForCustomer(request);
        }
    }

    @Override
    @RequestMapping(value = "", method = RequestMethod.POST)
    public OrderWrapper createNewCartForCustomer(HttpServletRequest request) {
        return super.createNewCartForCustomer(request);
    }

    @Override
    @RequestMapping(value = "/{cartId}/item", method = RequestMethod.POST, consumes = MediaType.ALL_VALUE)
    public OrderWrapper addItemToOrder(HttpServletRequest request,
                                       @PathVariable("cartId") Long cartId,
                                       @RequestBody OrderItemWrapper orderItemWrapper,
                                       @RequestParam(value = "priceOrder", required = false, defaultValue = "true") Boolean priceOrder) {
        return super.addItemToOrder(request, cartId, orderItemWrapper, priceOrder);
    }



     /*   Order cart = this.validateCartAndCustomer(cartId,customerId);

        try {
            if (orderItemWrapper.getProductId() == null && orderItemWrapper.getSkuId() == null && StringUtils.isBlank(orderItemWrapper.getName())) {
                throw BroadleafWebServicesException.build(400).addMessage("com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.invalidAddToCartRequest");
            } else {
                OrderItemRequestDTO orderItemRequestDTO = this.orderRequestService.populateOrderItemRequestDTO(request, orderItemWrapper);
                if (priceOrder == null) {
                    priceOrder = true;
                }

                Order order = this.orderService.addItemWithPriceOverrides(cart.getId(), orderItemRequestDTO, false);
                order = this.orderService.save(order, priceOrder);
                OrderWrapper wrapper = (OrderWrapper)this.context.getBean(OrderWrapper.class.getName());
                wrapper.wrapDetails(order, request);
                return wrapper;
            }
        } catch (PricingException var9) {
            throw BroadleafWebServicesException.build(500, (Locale)null, (Map)null, var9);
        } catch (AddToCartException var10) {
            throw BroadleafWebServicesException.build(500, (Locale)null, (Map)null, var10);
        }*/


   /* public Order validateCartAndCustomer(Long cartId,Long id){
        Order cart = this.orderService.findOrderById(cartId);
        if (cart != null && !(cart instanceof NullOrderImpl)) {
            Customer customer = customerDao.readCustomerById(id);
            if (customer != null && ObjectUtils.notEqual(customer.getId(), cart.getCustomer().getId())) {
                throw BroadleafWebServicesException.build(400).addMessage("com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.cartCustomerMismatch");
            } else {
                return cart;
            }
        } else {
            throw BroadleafWebServicesException.build(404).addMessage("com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.cartNotFound");
        }
    }*/
}

