package com.community.api.endpoint.customer;

import com.broadleafcommerce.rest.api.endpoint.customer.CustomerEndpoint;
import com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException;

import com.broadleafcommerce.rest.api.wrapper.CustomerWrapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;

import org.broadleafcommerce.profile.core.domain.Customer;
import org.broadleafcommerce.profile.web.core.CustomerState;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/customer",
        produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})

public class CustomCustomerEndpoint extends CustomerEndpoint {


    @RequestMapping(value = "/getCustomer", method = RequestMethod.GET)
    public CustomerWrapper findCustomerByEmail(HttpServletRequest request,
                                               @RequestParam(value = "email", required = true) String emailAddress,
                                               @RequestParam(value = "password", required = true) String password) {
        Customer customer = customerService.readCustomerByEmail(emailAddress);
        if (!customer.getPassword().equals(password)) {
            throw BroadleafWebServicesException.build(HttpStatus.SC_NOT_FOUND)
                    .addMessage(BroadleafWebServicesException.CUSTOMER_EMAIL_NOT_FOUND);
        }

       else if (customer == null) {
            throw BroadleafWebServicesException.build(HttpStatus.SC_NOT_FOUND)
                    .addMessage(BroadleafWebServicesException.CUSTOMER_EMAIL_NOT_FOUND);
        }
        if (CustomerState.getCustomer() == null) {
            CustomerState.setCustomer(customer);

        }
        CustomerWrapper response = (CustomerWrapper) context.getBean(CustomerWrapper.class.getName());
        response.wrapDetails(customer, request);

        return response;
    }

    @RequestMapping(value = "/addCustomer", method = RequestMethod.POST)
    public CustomerWrapper addCustomer(HttpServletRequest request,
                                       @RequestBody CustomerWrapper wrapper,
                                       @RequestParam(value = "password") String password) {
        Customer customer = wrapper.unwrap(request, context);
        if (StringUtils.isEmpty(customer.getUsername())) {
            // if no username was specified, we will use the email address instead
            String userName = (StringUtils.isNotBlank(customer.getEmailAddress()) ? customer.getEmailAddress() : "");
            customer.setUsername(userName);

        }
        customer.setPassword(password);
        customer = customerService.saveCustomer(customer);

        CustomerWrapper response = (CustomerWrapper) context.getBean(CustomerWrapper.class.getName());
        response.wrapDetails(customer, request);

        return response;
    }

}
